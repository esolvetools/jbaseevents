**Create events or jobs to be run in backgroud in jBase**

jBase is used in various industries and there comes a need where you want to run the certain commands or process in the background and update status on completion.

This is released under MIT license from eSolve Technlogies.

There is not easy eay to create a asyncronous running and checking of job/event status
---

## eStjBaseEvents

eStjBaseEvents are based on the jbase trigger mechanism.

1) EST.JB.EVENTS is the file which has all the requested jobs/queues
2) It has a PREWRITE trigger which starts the Trigger.
3) Trigger will start the required process, which can be either a shell command or another jbase subroutine to be called or a script (either python or shell or perl script)
4) the event to start in background can be done with EST.RAISE.EVENT.
5) The event status can be checked with EST.STATUS.EVENT

The current supported event types are Shell command, jbase routine to be called or script. Ability to cancel or delelte a scheduled activity will be added.



## Installation

clone the repository and run the command setup.sh. This has been tested in *nix environments. 

To raise the event simply call EST.RAISE.EVENT with following arguments

**EVENT.TYPE: CMD: To execute as is OR
            ROUTINE:Jbase subroutine to be called with arguments given in Fron EVENT.TYPE<2> OR
            SCRIPT: to execute python pr perl or shell script whch can send response back and stored in record<5>
            DELETE: If received the process will try to delete the record if already not done or cancel the process if under process (for future)**
**EVENT.ID  : If passed written with it, if not generated and returned**
**CMD:        Actual command or subroutine or script name**
**SCHED.DATE: Null for TODAY or future date**
**SCHED.TIME: Null for current time or future time**
**OTHER.INFO: For future use**
**ERR       : If any to be returned.**



## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
